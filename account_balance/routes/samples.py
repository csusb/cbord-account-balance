import base64
from flask import render_template
from .. import app
from ..models import Patron, PatronAccount
from ..routes.home import plan_displaynames, tender_displaynames
import datetime

@app.route('/samples')
def samples_home():
    return  render_template('samples.html')

@app.route('/samples/unknown-user')
def unknown_user():
    return  render_template('unknown-user.html'), 403 #Forbidden

@app.route('/samples/missing-account')
def missing_account():
    return render_template('nodata.html',eppn='99999999@example.edu')

@app.route('/samples/example_user')
def example_user():
    patron = Patron(First_Name="Example",Last_Name="Student",ID_Number="987654321",
                    Plan="FS Block 15 & 10 DD", Plan_ID="1",
                    Last_Error="Account Balance too low to complete sale")
    accounts = []
    accounts.append( PatronAccount(
       Amount_Spent = 18.65,
       Balance = 0.35,
       Charge_Limit = 0,
       Date_Time_Last_Used = datetime.datetime(2019, 5, 1, 16, 28, 30, 36),
       Department_SK_FK = None,
       Net_Debits_Credits = 19.00,
       Net_Sales = 18.65,
       On_Hold = 0,
       Overdraft_Date = None,
       Repeat_Count = 0,
       Tender = 'Flex Cash',
       Tender_ID = 4,
       Terminal_ID_Last_Used = None,
       Terminal_Last_Used = None,
       Total_Reset_Date = None,
       Usage_Count = 24
    ) )
    accounts.append( PatronAccount(
       Amount_Spent = 7.00,
       Balance = 8.00,
       Charge_Limit = 0,
       Date_Time_Last_Used = datetime.datetime(2019, 5, 2, 11, 16, 10, 42),
       Department_SK_FK = None,
       Net_Debits_Credits = 15.00,
       Net_Sales = 7.00,
       On_Hold = 0,
       Overdraft_Date = None,
       Repeat_Count = 0,
       Tender = 'Board',
       Tender_ID = 3,
       Terminal_ID_Last_Used = None,
       Terminal_Last_Used = None,
       Total_Reset_Date = None,
       Usage_Count = 3,
    ) )
    picture_url = None
    with app.open_resource('static/nathillardheadshot.jpg') as f:
        binary = f.read()
        b64 = base64.b64encode(binary)
        b64 = b64.decode("utf-8")
        picture_url = 'data:image/jpeg;base64,' + b64
    return render_template('home.html',
                           patron=patron,
                           accounts=accounts,
                           plan_displaynames=plan_displaynames,
                           tender_displaynames=tender_displaynames,
                           title="Card Balance",
                           picture_url=picture_url)
