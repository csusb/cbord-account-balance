import base64
import io
import json
from functools import wraps

from flask import jsonify,redirect,render_template,request,send_file,session,url_for
from .. import app
from ..models import Patron,PatronPicture
from .. import barcode

try:
    import uwsgi
except ModuleNotFoundError:
    pass

tender_displaynames = {}
with app.open_instance_resource('tenders.json') as f:
    seralized = json.load(f)
    #json doesn't support integers as keys
    #so they need to be converted
    tender_displaynames = {int(key):value for key,value in seralized.items()}

plan_displaynames = {}
with app.open_instance_resource('plans.json') as f:
    seralized = json.load(f)
    plan_displaynames = {int(key):value for key,value in seralized.items()}

authorized_agents = []
try:
    with app.open_instance_resource('agents.json') as f:
        authorized_agents = json.load(f)
except IOError:
    pass

def authenticate(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if 'SAML_BYPASS_AS' in app.config:
            session['saml'] = { 'attributes': {
                                'eduPersonPrincipalName': [ app.config['SAML_BYPASS_AS'] ]}}

        if 'saml' not in session:
            return redirect(url_for('login'))

        try:
            session['eppn'] = session['saml']['attributes']['eduPersonPrincipalName'][0]
        except KeyError:
            return  render_template('unknown-user.html'), 403 #Forbidden

        try:
            username, _ = session['eppn'].split('@')
            session['username'] = username
        except:
            return render_template('unknown-user.html'), 403 #Forbidden

        try:
            #having a tough time setting %(user), either directly or via
            #env['REMOTE_USER'] #relies on
            #https://uwsgi-docs.readthedocs.io/en/latest/LogFormat.html#user-defined-logvars
            #using repr() to try to dodge a logging vulnerability. hopefully by this
            #point it's a reasonable value
            uwsgi.set_logvar('eppn',repr(session['eppn']))
        except NameError:
            #we're not running under uwsgi
            pass
        return f(*args, **kwargs)
    return wrapper

@app.route('/picture')
@authenticate
def picture():
    picture_url = url_for('static', filename="picture-missing.png")

    if not session.get('principal'):
        #return the placeholder if shenanigans 
        return jsonify(picture_url=picture_url)

    picture = PatronPicture.query.filter_by(ID_Number=session['principal']).first()
    try:
        picture_url = "data:image/jpeg;base64," + base64.b64encode(picture.Picture).decode("utf-8")
    except Exception:
        pass
    return jsonify(picture_url=picture_url)

@app.route('/codabar/<code>')
@app.route('/codabar/<code>/rotate/<int:degrees>')
@authenticate
def codabar(code,degrees=0,barwidth=2):
    buffer = io.BytesIO()
    color = request.args.get('color')
    try:
       bc = barcode.Codabar("A" + code + "A", color=color,barwidth=barwidth,height=50)
    except ValueError:
       return "Image not found", 404
    if degrees:
        rotated = bc.img.rotate(degrees,expand=1)
        rotated.save(buffer,"PNG")
    else:
        bc.img.save(buffer,format="PNG",dpi=(600,600))
    buffer.seek(0)
    return send_file(buffer, mimetype='image/png')

def most_recent_date_time(a,b):
    if a is None:
        a = ''
    if b is None:
        b = ''
    return a if a > b else b

@app.route('/')
@app.route('/patron/<patronid>')
@authenticate
def home(patronid=""):
    session['principal'] = session['username']
    agent = False
    if session['eppn'] in authorized_agents:
       agent = True
    app.logger.debug(f'Patron ID is {patronid}')
    if patronid:
        if not agent:
            app.logger.warning("%s attempted to look up patron %s",session['eppn'],patronid)
            return redirect(url_for('home'))
        app.logger.info("Agent %s is viewing patron %s",session['eppn'],patronid)
        session['principal'] = patronid

    patron = Patron.query.filter_by(ID_Number=session['principal']).first()
    if not patron:
        return render_template('nodata.html',username=session['principal'],patronid=patronid,agent=agent)
    if not len(patron.accounts):
        return render_template('nodata.html',username=session['principal'],patronid=patronid,agent=agent)

    accounts = {account.Tender_ID:account for account in patron.accounts}

    #for each tender to be displayed
    #    add each of its fallback tender balances to the displayed total
    #    then trash the fallback tender
    for display_id,attributes in tender_displaynames.items():
        try:
            fallback_tender_ids = [int(_id) for _id in attributes['fallbacks']]
        except KeyError:
            continue

        if display_id not in accounts:
            #hmmm, no primary account.  Maybe only legacy?
            for _id in fallback_tender_ids:
                if _id in accounts:
                   accounts[display_id] = accounts[_id]
                   accounts[display_id].Tender_ID = display_id
                   app.logger.info(f'Patron ID {patronid} substituting missing tender={display_id} for tender={_id}')
                   del accounts[_id]
                   break
            
        for fallback_id in fallback_tender_ids:
            try:
                display = accounts[display_id]
                fallback = accounts[fallback_id]
            except KeyError:
                #No subsitute for the "primary" display_id could be found
                #the fallback account may not exist
                continue
            display.Balance += fallback.Balance
            display.Date_Time_Last_Used = most_recent_date_time(display.Date_Time_Last_Used,
                                                                fallback.Date_Time_Last_Used)

            del accounts[fallback_id]

    #2nd filter, keep only those tenders specified in the tenders.json display file
    accounts = {_id:accounts[_id] for _id in accounts if _id in tender_displaynames}

    #3rd filter, remove untouched tenders
    accounts = {_id:accounts[_id] for _id in accounts if accounts[_id].Date_Time_Last_Used 
                   or accounts[_id].Balance != 0.0}

    #try to warn about fractional meal swipes
    swipe_fraction = None
    for a in accounts.values():
       try:
           unit = tender_displaynames[a.Tender_ID]["unit"]
       except KeyError:
           continue
       _, fraction = "{:.2f}".format(a.Balance).split('.')
       if fraction != "00":
           swipe_fraction = "{} for {}".format(fraction,tender_displaynames[a.Tender_ID]["display"])
           break

    title = "Card Balance for {} {}".format(patron.First_Name, patron.Last_Name)

    return render_template('home.html',
                           patron=patron,
                           accounts=accounts,
                           tender_displaynames=tender_displaynames,
                           plan_displaynames=plan_displaynames,
                           swipe_fraction=swipe_fraction,
                           title=title,
                           agent=agent,
                           patronid=patronid)

@app.route('/query-patron',methods=['POST'])
def lookup():
   patronid = request.form.get('patron',None)
   if not patronid:
       return redirect(url_for('home'))
   return redirect(url_for('home',patronid=patronid))
