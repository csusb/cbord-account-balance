from PIL import Image, ImageDraw, ImageFont, ImageColor

class Barcode():
    def __init__(self,code,height=40,barwidth=1,margin=10,color=None,background=None):
        self.code = code
        self.validate()

        self.height = height
        self.barwidth = barwidth
        self.margin = margin
        size = (self.width + 2*self.margin), (self.height + 2*self.margin)
        if background is None:
            self.background = ImageColor.getrgb("white")
        else:
            self.background = ImageColor.getrgb(background)
        if color is None:
            self.color = ImageColor.getrgb("black")
        else:
            self.color = ImageColor.getrgb(color)
        self.img = Image.new("RGB", size, self.background)
        self.draw()


    def validate(self):
        raise NotImplementedError

    def draw(self):
        raise NotImplementedError

    @property
    def width(self):
        raise NotImplementedError

class Codabar(Barcode):
    ratio = 3 #between 1:2.25 and 1:3
    encodings = {
             #Bars      #Spaces
        '0': ((0,0,0,1), (0,0,1,0)),
        '1': ((0,0,1,0), (0,0,1,0)),
        '4': ((0,1,0,0), (0,0,1,0)),
        '5': ((1,0,0,0), (0,0,1,0)),
        '2': ((0,0,0,1), (0,1,0,0)),
        '-': ((0,0,1,0), (0,1,0,0)),
        '$': ((0,1,0,0), (0,1,0,0)),
        '9': ((1,0,0,0), (0,1,0,0)),
        '6': ((0,0,0,1), (1,0,0,0)),
        '7': ((0,0,1,0), (1,0,0,0)),
        '8': ((0,1,0,0), (1,0,0,0)),
        '3': ((1,0,0,0), (1,0,0,0)),

        'C': ((0,0,0,1), (0,1,1,0)),
        '*': ((0,0,0,1), (0,1,1,0)),
        'D': ((0,0,1,0), (0,1,1,0)),
        'E': ((0,0,1,0), (0,1,1,0)),
        'A': ((0,1,0,0), (0,1,1,0)),
        'T': ((0,1,0,0), (0,1,1,0)),
        'B': ((0,0,0,1), (1,1,0,0)),
        'N': ((0,0,0,1), (1,1,0,0)),

        '.': ((1,1,1,0), (0,0,0,0)),
        '/': ((1,1,0,1), (0,0,0,0)),
        ':': ((1,0,1,1), (0,0,0,0)),
        '+': ((0,1,1,1), (0,0,0,0)),
    }

    def charwidth(self, character):
        e = self.encodings[character]
        width = 0
        for bar_is_wide,space_is_wide in zip(*e):
            width += self.barwidth * self.ratio if bar_is_wide else self.barwidth
            width += self.barwidth * self.ratio if space_is_wide else self.barwidth
        return width

    def renderchar(self,character,start):
        pass

    def validate(self):
        valid_end = valid_start = list("ABCDEN*T")
        valid = list("0123456789$:/+.")
        if len(self.code) < 3:
            raise ValueError("Code length")
        if self.code[0] not in valid_start:
            raise ValueError("Code start value invalid")
        if self.code[-1] not in valid_end:
            raise ValueError("Code end value invalid")
        for c in self.code[1:-1]:
            if c not in valid:
                raise ValueError(f"Invalid code value {c}")

    @property
    def width(self):
        return sum([self.charwidth(c) for c in self.code])

    def draw(self):
        start = self.margin
        for c in self.code:
            e = self.encodings[c]

            for bar_is_wide,space_is_wide in zip(*e):
                d = ImageDraw.Draw(self.img)

                barwidth = self.barwidth * self.ratio if bar_is_wide else self.barwidth
                spacewidth = self.barwidth * self.ratio if space_is_wide else self.barwidth

                top_left = (start,self.margin)
                bottom_right = (start+barwidth-1,self.margin+self.height)
                d.rectangle([top_left,bottom_right],fill=self.color,width=0)
                start = start + barwidth + spacewidth


if __name__ == '__main__':
     bc = Codabar("A123456789$:+/A",barwidth=2, height=80, margin=10)
     print(bc.charwidth('3'))
     print(bc.charwidth('A'))
     print(bc.width)
     rotated = bc.img.rotate(270,expand=1)
     rotated.save('testing.svg')