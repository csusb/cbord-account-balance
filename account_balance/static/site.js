
  function fetch_picture(url) {
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.onload = function() {
      if (this.status >= 200 && this.status < 400) {
          var picture_info = JSON.parse(this.response);
          photo_img = document.querySelector('#patron-picture');
          photo_img.setAttribute('src',picture_info['picture_url']);
      } else {
          // We reached our target server, but it returned an error
      }
    };

    request.onerror = function() {
        // There was a connection error of some sort
    };

    request.send();
  }

  function runProgressBar(blockselector, runtime, callback=null, remaining = null) {
      let pb = document.querySelector(`${blockselector} .progress-bar`);
      let increment = 100;
      if (remaining === null) {
          remaining = runtime;
      }
      if(remaining > 0) {
          pb.style.width = remaining/runtime*100 + "%";
          remaining -= increment;
          window.setTimeout(() => {runProgressBar(blockselector, runtime, callback, remaining);}, increment);
      } else {
          pb.style.width = "100%";
          if (!(callback === null)) {
              callback()
          }
      }
  }


  window.addEventListener('load', (event) => {
    btn = document.querySelector("#patron-details-button button");
    if (btn === null) { return; }

    btn.addEventListener('click', (event) => {
          let runtime = 20000;
          $("#patron-details").show();
          btn.disabled = true;
          runProgressBar("#patron-details", runtime, () => {
                $("#patron-details").hide();
                btn.disabled = false;
          });
      });
  });