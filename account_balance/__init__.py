import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_saml import FlaskSAML

# create and configure the app
app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)
FlaskSAML(app)

from .routes import home
from .routes import samples
from .routes import errors
