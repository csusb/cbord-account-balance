from datetime import datetime,date
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from . import db

class Patron(db.Model):
    __tablename__ = 'AV_User_PCS_Patron'

    #'ID_Number','varchar(250)',0,0
    ID_Number = db.Column(db.String(250),unique=True)
    #'Card_Number','varchar(250)',1,0
    Card_Number = db.Column(db.String(250))
    #'Alternate_ID_Number','varchar(250)',1,0
    Alternate_ID_Number = db.Column(db.String(250))
    #'First_Name','varchar(250)',1,0
    First_Name= db.Column(db.String(250))
    #'Middle_Name','varchar(250)',1,0
    Middle_Name = db.Column(db.String(250))
    #'Last_Name','varchar(250)',1,0
    Last_Name = db.Column(db.String(250))
    #'Gender','varchar(250)',1,0
    Gender = db.Column(db.String(250))
    #'Birth_Date','date',1,0
    Birth_Date = db.Column(db.DateTime, default=datetime.min)
    #'Building','varchar(250)',1,0
    Building = db.Column(db.String(250))
    #'Building_ID','smallint',1,0
    Building_ID = db.Column(db.Integer)
    #'Mail_Box','varchar(250)',1,0
    Mail_Box = db.Column(db.String(250))
    #'Email','varchar(250)',1,0
    Email = db.Column(db.String(250))
    #'Website','varchar(250)',1,0
    Website = db.Column(db.String(250))
    #'Patron_Message','varchar(250)',1,0
    Patron_Message = db.Column(db.String(250))
    #'Message_ID','smallint',1,0
    Message_ID = db.Column(db.Integer)
    #'Notes','long varchar',1,0
    Notes = db.Column(db.Text)
    #'PCS_Add_Date','timestamp',1,0
    PCS_Add_Date = db.Column(db.DateTime, default=datetime.min)
    #'Plan','varchar(250)',1,0
    Plan = db.Column(db.String(250))
    #'Plan_ID','smallint',1,0
    Plan_ID = db.Column(db.Integer)
    #'Previous_Plan','varchar(250)',1,0
    Previous_Plan = db.Column(db.String(250))
    #'Previous_Plan_ID','smallint',1,0
    Previous_Plan_ID = db.Column(db.Integer)
    #'ActivityGroup','varchar(250)',1,0
    ActivityGroup = db.Column(db.String(250))
    #'ActivityGroup_ID','smallint',1,0
    ActivityGroup_ID = db.Column(db.Integer)
    #'Department','varchar(250)',1,0
    Department = db.Column(db.String(250))
    #'Department_ID','smallint',1,0
    Department_ID = db.Column(db.Integer)
    #'Max_Checkouts','smallint',1,0
    Max_Checkouts = db.Column(db.Integer)
    #'Privilege_Expiration_Date','date',1,0
    Privilege_Expiration_Date = db.Column(db.Date, default=date.min)
    #'Last_Date_Card_Used','timestamp',1,0
    Last_Date_Card_Used = db.Column(db.DateTime, default=datetime.min)
    #'Last_Error','varchar(250)',1,0
    Last_Error = db.Column(db.String(250))
    #'Last_Error_Abbreviation','varchar(250)',1,0
    Last_Error_Abbreviation = db.Column(db.String(250))
    #'ADM_Used','numeric(10,2)',1,0
    ADM_Used = db.Column(db.Numeric(10,2))
    #'ADM_Used_Date','timestamp',1,0
    ADM_Used_Date = db.Column(db.DateTime(), default=datetime.min)
    #'Card_Lost','numeric(1,0)',0,0
    Card_Lost = db.Column(db.Numeric(1,0))
    #'Cards_Replaced','smallint',1,0
    Cards_Replaced = db.Column(db.Integer)
    #'Card_On_Hold','numeric(1,0)',0,0
    Card_On_Hold = db.Column(db.Numeric(1,0))
    #'Card_On_Vend_Hold','numeric(1,0)',0,0
    Card_On_Vend_Hold = db.Column(db.Numeric(1,0))
    #'Vend_Hold_Activated','timestamp',1,0
    Vend_Hold_Activated = db.Column(db.DateTime, default=datetime.min)
    #'Patron_SK','integer',0,0
    Patron_SK = db.Column(db.Integer,primary_key=True)
    #'Alternate_Card_Number','varchar(250)',1,0
    Alternate_Card_Number = db.Column(db.String(250))
    #'Last_Date_Card_Issued','timestamp',1,0
    Last_Date_Card_Issued = db.Column(db.DateTime, default=datetime.min)

    accounts = db.relationship('PatronAccount',backref='Patron_SK',lazy=True)

    def __repr__(self):
        return '<Patron {}, {}, {}, {}>'.format(self.ID_Number, self.First_Name, self.Last_Name, self.Last_Date_Card_Used)

class PatronAccount(db.Model):
    __tablename__ = 'AV_User_PCS_PatronAccount'

    #'Tender','varchar(250)',1,0
    Tender = db.Column(db.String(250))
    #'Tender_ID','smallint',0,0
    Tender_ID = db.Column(db.Integer,primary_key=True)
    #'Balance','numeric(10,2)',1,0
    Balance = db.Column(db.Numeric(10,2))
    #'Charge_Limit','numeric(10,2)',1,0
    Charge_Limit = db.Column(db.Numeric(10,0))
    #'On_Hold','numeric(1,0)',0,0
    On_Hold = db.Column(db.Numeric(1,0))
    #'Date_Time_Last_Used','timestamp',1,0
    Date_Time_Last_Used = db.Column(db.DateTime)
    #'Terminal_Last_Used','varchar(250)',1,0
    Terminal_Last_Used = db.Column(db.String(250))
    #'Terminal_ID_Last_Used','smallint',1,0
    Terminal_ID_Last_Used = db.Column(db.Integer)
    #'Net_Sales','numeric(12,2)',1,0
    Net_Sales = db.Column(db.Numeric(12,2))
    #'Usage_Count','integer',1,0
    Usage_Count = db.Column(db.Integer)
    #'Repeat_Count','integer',1,0
    Repeat_Count = db.Column(db.Integer)
    #'Amount_Spent','numeric(10,2)',1,0
    Amount_Spent = db.Column(db.Numeric(10,2))
    #'Net_Debits_Credits','numeric(12,2)',1,0
    Net_Debits_Credits = db.Column(db.Numeric(12,2))
    #'Total_Reset_Date','date',1,0
    Total_Reset_Date = db.Column(db.Date)
    #'Overdraft_Date','timestamp',1,0
    Overdraft_Date = db.Column(db.DateTime)
    #'Patron_SK_FK','integer',1,0
    Patron_SK_FK = db.Column(db.Integer,db.ForeignKey('AV_User_PCS_Patron.Patron_SK'),primary_key=True)
    #'Department_SK_FK','integer',1,0
    # Flagging this foreign key as primary_key as this view isn't exposing PatronAccount_SK
    Department_SK_FK = db.Column(db.Integer)

class PatronPicture(db.Model):
    __tablename__ = 'av_user_csd_PatronPicture'
    #varchar(250)
    ID_Number = db.Column(db.String(250),primary_key=True)
    #long binary
    Picture = db.Column(db.LargeBinary())


