FROM python:3.7-slim as compile-image
RUN apt-get update
RUN apt-get install -y --no-install-recommends build-essential gcc

WORKDIR /opt/app

RUN python -m venv /opt/app/venv
# Make sure we use the virtualenv:
ENV PATH "/opt/app/venv/bin:$PATH"


COPY requirements.txt ./

RUN pip install --no-cache-dir --upgrade pip wheel
RUN pip install --no-cache-dir -r requirements.txt

FROM python:3.7-slim AS build-image


RUN apt-get update -q \
        && apt-get install -y --no-install-recommends xmlsec1 \
        && find /var/lib/apt/lists -type f -delete

WORKDIR /opt/app
COPY --from=compile-image /opt/app/venv /opt/app/venv
COPY account_balance/*.py ./account_balance/
COPY account_balance/static ./account_balance/static
COPY account_balance/templates ./account_balance/templates
COPY account_balance/routes ./account_balance/routes
COPY instance ./instance
COPY sqlanywhere17  /opt/sqlanywhere17
COPY app.py ./

ENV PATH "/opt/app/venv/bin:$PATH"
COPY tests/ tests/
COPY create_db.py .
COPY config.ini .
RUN  /opt/app/venv/bin/python create_db.py

ENV LD_LIBRARY_PATH /opt/sqlanywhere17/lib64

USER www-data
ENTRYPOINT [ "uwsgi", "--ini", "config.ini" ]
#default port 3031 seems to be unliked in the version of mod_proxy_uwsgi
CMD ["--socket", ":3032", "--processes", "4", "--threads", "2" ]
