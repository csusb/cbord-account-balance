import os
from account_balance import app

def main():
    from account_balance import db
    db.create_all()

    with open(os.path.join('tests', 'data.sql'),'rb') as f_data, \
         open(os.path.join('tests', 'pictures.sql'),'rb') as f_pictures:
        _data_sql = f_data.read().decode('utf8')
        _images_sql = f_pictures.read().decode('utf8')
        connection = db.engine.raw_connection()
        try:
           cursor = connection.cursor()
           cursor.executescript(_data_sql)
           cursor.executescript(_images_sql)
           cursor.close()
           connection.commit()
        finally:
           connection.close()

if __name__ == '__main__':
    if app.config['SQLALCHEMY_DATABASE_URI'].startswith("sqlite://"):
        main()
