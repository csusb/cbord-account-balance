
Notes
-----

needs os packages, e.g.

   - `apt install xmlsec1`

   - sqlanywhere https://wiki.scn.sap.com/wiki/display/SQLANY/SAP+SQL+Anywhere+Database+Client+Download  
     `./setup -install sqlany_client64`  
     United States: `30`  
     Devel web server needs to find the libdbcsql_r.so via  
     `LD_LIBRARY_PATH=/opt/sqlanywhere17/lib64 ... flask run`


SAML entityID is dynamically generated based on HTTP `Host:`, so
http://127.0.0.1:5000/ and http://localhost:1234/ will generate
`entityID="http://127.0.0.1:5000/saml/metadata/"` or
`entityID="http://localhost:1234/saml/metadata/"` respectfully. As such, you may
want to add `FLASK_RUN_PORT=8080` in your enviornment.

Development Installation
------------------------

1. Clone this repo:  
`git clone [...]/cboard-account-balance.git`

1. Change working directory:  
`cd cboard-account-balance`

1. Make a python3 >= 3.5 virtual env:  
`python3 -m venv venv`

1. Activate that virtual env. You should get a (venv) prompt after:  
`source venv/bin/activate`

1. Install requirements (into the venv):  
`pip install -r requirements.txt`

1. Install `xmlsec1` OS package, e.g. for Debian/Ubuntu:  
`sudo apt install xmlsec1`

1. Create a `account_balance/instance/config.py` (use the `config.py.example`)
    1. also look at the `agents.json` which contains a list of accounts authorized to lookup other accounts

1. Create the dev database with `python create_db.py`

1. Launch the development web server:  
`FLASK_APP=account_balance FLASK_ENV=development flask run`  
or  
    ```
    #Powershell
    $Env:FLASK_APP="account_balance"
    $Env:FLASK_ENV="development"
    $Env:FLASK_RUN_PORT=8080
    flask run
    ```

1. Full example with sqlanywhere...  

    ```
    SQLALCHEMY_DATABASE_URI='sqlalchemy_sqlany://user:password@localhost:2638/Database' \
    FLASK_APP=account_balance \
    FLASK_ENV=development \
    FLASK_SECRET=badsecret \
    SAML_METADATA_URL=https://weblogon.dev.csusb.edu/idp/shibboleth \
    LD_LIBRARY_PATH=/opt/sqlanywhere17/lib64 \
    flask run
    ```



SAML
----

1. Review the Identity Provider (IdP) metadata url in `account_balance/instance/config.py`

1. Get a generated Service Provider (SP) metadata from the `/saml/metadata/` path, e.g. http://localhost:5000/saml/metadata/

1. Register the SP metadata with the IdP (for CSUSB, check the FAQ document pinned to the csusbit.slack.com #iam channel)

1. Request `eduPersonPrincipalName` in the attribute release policy  
    ```xml  
    <!-- [...] -->  
      <AttributeFilterPolicy id="my-flask-dev">  
        <PolicyRequirementRule xsi:type="basic:AttributeRequesterString" value="http://localhost:5000/saml/metadata/"/>  
        <AttributeRule attributeID="eduPersonPrincipalName">  
          <PermitValueRule xsi:type="basic:ANY"/>  
        </AttributeRule>  
      </AttributeFilterPolicy>  
    <!-- [...] -->  
    ```


Docker
------
Needs /opt/sqlanywhere17 copied to the build directory  
`cp -r /opt/sqlanywhere17 .`

`docker-compose up` is intended to create a contianer with an Apache instance listining on localhost:8080 with mod_proxy_uwsgi, and another container  running this app under uwsgi.