for col in "ID_Number Card_Number Alternate_ID_Number First_Name Middle_Name Last_Name Gender Birth_Date Building Building_ID Mail_Box" \
    " Email Website Patron_Message Message_ID Notes PCS_Add_Date Plan Plan_ID Previous_Plan Previous_Plan_ID ActivityGroup" \
    " ActivityGroup_ID Department Department_ID Max_Checkouts Privilege_Expiration_Date Last_Date_Card_Used Last_Error" \
    " Last_Error_Abbreviation ADM_Used ADM_Used_Date Card_Lost Cards_Replaced Card_On_Hold Card_On_Vend_Hold Vend_Hold_Activated" \
    " Patron_SK Alternate_Card_Number Last_Date_Card_Issued".split() :

     print("UPDATE AV_User_PCS_Patron SET {0} = null WHERE {0} = '';".format(col))


for col in ("Tender", "Tender_ID", "Balance", "Charge_Limit", "On_Hold", "Date_Time_Last_Used",
        "Terminal_Last_Used", "Terminal_ID_Last_Used", "Net_Sales", "Usage_Count", "Repeat_Count",
        "Amount_Spent", "Net_Debits_Credits", "Total_Reset_Date", "Overdraft_Date", "Patron_SK_FK",
        "Department_SK_FK"):

     print("UPDATE AV_User_PCS_PatronAccount SET {0} = null WHERE {0} = '';".format(col))
