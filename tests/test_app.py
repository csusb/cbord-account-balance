import warnings
import functools
import re

import pytest
from account_balance.models import Patron
from sqlalchemy.exc import SAWarning

from flask import session

def suppress_sqlite_warnings(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        with warnings.catch_warnings():
            #This is fine for dev/testing.  Production is sybase dialect
            warnings.filterwarnings('ignore',
                r"^Dialect sqlite\+pysqlite does \*not\* support Decimal objects",
                SAWarning)
            return func(*args, **kwargs)
    return wrapper

def test_sp_init_sso(app):
    with app.test_client() as client:
       rv = client.get('/')
       assert rv.status_code == 302

def test_redrect_unauth_picture(app):
    with app.test_client() as client:
       rv = client.get('/picture')
       assert rv.status_code == 302

def test_redrect_unauth_agent_lookup(app):
    with app.test_client() as client:
       rv = client.get('/patron/foobar')
       assert rv.status_code == 302

@suppress_sqlite_warnings
def test_authenticated_balance(app):
    with app.test_client() as c:
        with c.session_transaction() as sess:
            #fake saml response in session
            sess['saml'] = {
                'subject': 'fooBarBuzBaz',
                'attributes': { 'eduPersonPrincipalName':
                                ['000226420@example.edu'] }}
        rv = c.get('/')
        assert rv.status_code == 200
        assert b'Joe Coyote' in rv.data
        assert re.search(rb'12\s+swipes',rv.data) is not None
        assert re.search(rb'\$31\.14',rv.data)

@suppress_sqlite_warnings
def test_account_warning (app):
    with app.test_client() as c:
        with c.session_transaction() as sess:
            #fake saml response in session
            sess['saml'] = {
                'subject': 'fooBarBuzBaz',
                'attributes': { 'eduPersonPrincipalName':
                                ['888888888@example.edu'] }}
        rv = c.get('/')
        assert rv.status_code == 200
        assert b'Philip Fry' in rv.data
        assert b'Account Balance too low' in rv.data
        assert b'alert alert-warning' in rv.data

@suppress_sqlite_warnings
def test_patron_lookup_by_id_number(app):
    with app.app_context():
        p =  Patron.query.filter_by(ID_Number='000030081')
        assert p.first() is None

        p =  Patron.query.filter_by(ID_Number='000226420').first()
        assert p is not None
        assert p.First_Name == "Joe"

@suppress_sqlite_warnings
def test_unknown_patron(app):
    with app.test_client() as c:
        with c.session_transaction() as sess:
            #fake saml response in session
            sess['saml'] = {
                'subject': 'fooBarBuzBaz',
                'attributes': { 'eduPersonPrincipalName':
                                ['DoesNotExist@example.edu'] }}
        rv = c.get('/')
        assert rv.status_code == 200
        assert b'Missing Balance' in rv.data

@suppress_sqlite_warnings
def test_patron_but_no_accounts(app):
    with app.test_client() as c:
        with c.session_transaction() as sess:
            #fake saml response in session
            sess['saml'] = {
                'subject': 'fooBarBuzBaz',
                'attributes': { 'eduPersonPrincipalName':
                                ['222222222@example.edu'] }}
        rv = c.get('/')
        assert rv.status_code == 200
        assert b'Missing Balance' in rv.data

@suppress_sqlite_warnings
def test_non_agent_lookup_attempt(app):
    with app.test_client() as c:
        with c.session_transaction() as sess:
            #fake saml response in session
            sess['saml'] = {
                'subject': 'fooBarBuzBaz',
                'attributes': { 'eduPersonPrincipalName':
                                ['222222222@example.edu'] }}
        rv = c.get('/patron/some-patron-id-here')
        assert rv.status_code == 302

@suppress_sqlite_warnings
def test_agent_lookup(app):
    with app.test_client() as c:
        with c.session_transaction() as sess:
            #fake saml response in session
            sess['saml'] = {
                'subject': 'fooBarBuzBaz',
                'attributes': { 'eduPersonPrincipalName':
                                ['admin@example.edu'] }}
        rv = c.get('/patron/888888888')
        assert rv.status_code == 200
        assert b'Philip Fry' in rv.data

@suppress_sqlite_warnings
def test_agent_lookup_of_nonexistant_patron(app):
    with app.test_client() as c:
        with c.session_transaction() as sess:
            #fake saml response in session
            sess['saml'] = {
                'subject': 'fooBarBuzBaz',
                'attributes': { 'eduPersonPrincipalName':
                                ['admin@example.edu'] }}
        rv = c.get('/patron/faky.mcfakerson1')
        assert rv.status_code == 200
        assert b'Missing Balance' in rv.data

@suppress_sqlite_warnings
def test_patron_image(app):
    with app.test_client() as c:
        with c.session_transaction() as sess:
            #fake saml response in session
            sess['saml'] = {
                'subject': 'fooBarBuzBaz',
                'attributes': { 'eduPersonPrincipalName':
                                ['admin@example.edu'] }}
        rv = c.get('/patron/888888888')
        assert rv.status_code == 200
        assert b'<img id="patron-picture"' in rv.data
        assert re.search(rb'<div id="patron-details" .{1,80}display: none',rv.data) is not None
        rv = c.get('/picture')
        assert rv.status_code == 200
        assert b'"picture_url": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQ' in rv.data
        assert b'elOXJNMhNbsv8R02vbdsovHmt180dA/RL6mHOOFKjsk8vPW4pcai1Oa77XYuInhPw2lLR+wpSawz5Dj0LpqwuCAIAgCAIAgCAIAgCAIAgCAID//Z' in rv.data

@suppress_sqlite_warnings
def test_patron_but_no_accounts(app):
    with app.test_client() as c:
        with c.session_transaction() as sess:
            #fake saml response in session
            sess['saml'] = {
                'subject': 'fooBarBuzBaz',
                'attributes': { 'eduPersonPrincipalName':
                                ['333333333@example.edu'] }}
        rv = c.get('/')
        assert rv.status_code == 200
        assert b'src="/static/picture-missing.png' in rv.data

