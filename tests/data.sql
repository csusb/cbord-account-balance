--All three target accounts
INSERT INTO AV_User_PCS_Patron
VALUES('000226420','0000000002264200',
       'Not Enrolled','Joe','W','Coyote',
       NULL,NULL,NULL,NULL,
       'Enrolled', 'coyotej@coyote.example.edu',
       NULL,NULL,NULL,NULL,
       '2018-05-07 06:42:43.670',
       '12 MPW & 300 DD',16,'Holding',999,
       NULL,NULL,NULL,NULL,NULL,NULL,
       '2019-05-06 15:09:45.061',
       NULL,NULL,0,NULL,0,1,0,0,NULL,123456, NULL,
       '2018-07-24 12:04:42.919');

INSERT INTO AV_User_PCS_PatronAccount VALUES(
    'Board',20,12,0,0,'2019-05-03 18:48:16.337',
    NULL,NULL,93,92,0,93,105,NULL,NULL,123456,NULL);

INSERT INTO AV_User_PCS_PatronAccount VALUES(
    'Points',3,31.14,0,0,'2019-05-06 15:09:45.061',
    NULL,NULL,868.86,139,0,868.86,900,NULL,NULL,123456,NULL);

INSERT INTO AV_User_PCS_PatronAccount VALUES(
    'Flex Cash',4,2.55,0,0,'2019-05-01 15:45:11.289',
    NULL,NULL,18.45,22,0,18.45,21,NULL,NULL,123456,NULL);

--needs MOAR fundz
INSERT INTO AV_User_PCS_Patron VALUES('888888888','0000008888888881',
        'Not Enrolled','Philip','J','Fry',
        NULL,NULL,NULL,NULL,
        'Enrolled','philip.fry1111@coyote.example.edu',
        NULL,NULL,NULL,NULL,
        '2018-05-07 06:44:03.189',
        'Database Import',1,
        NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
        '2019-04-30 17:01:53.640',
        'Account Balance too low to complete sale','LOW BALANCE',
        0,NULL,0,1,0,0,NULL,111111,NULL,'2018-06-02 08:53:43.654');

INSERT INTO AV_User_PCS_PatronAccount VALUES(
    'Flex Cash',4,0,0,0,'2019-04-30 17:00:12.465',
    NULL,NULL,30,8,0,30,30,NULL,NULL,111111,NULL);

--Leela doesn't have any associated accounts
INSERT INTO AV_User_PCS_Patron VALUES('222222222','000002222222221',
    NULL,'Turanga',NULL,'Leela',NULL,NULL,NULL,NULL,NULL,
   'TLeela@example.edu',NULL,NULL,NULL,
   '{\\rtf1\\ansi\\deff0\\deftab720{\\fonttbl{\\f0\\fswiss MS Sans Serif;}{\\f1\\froman\\fcharset2 Symbol;}}\x0d\x0a{\\colortbl\\red0\\green0\\blue0;}\x0d\x0a\\deflang1033\\pard\\plain\\f0\\fs20 \x0d\x0a\\par }\x0d\x0a',
   '2004-09-15 11:36:25.000',
   'FS Block 30 & 20 DD',24,'Students/Staff/Faculty',
   3,NULL,NULL,NULL,NULL,NULL,NULL,
   '2019-05-02 12:06:32.947',NULL,NULL,0,NULL,0,0,0,0,NULL,12121,NULL,NULL);

-- Pretty much me, just with IDs munged
-- Lots of zero-blaance, never-used accounts
INSERT INTO AV_User_PCS_Patron VALUES('333333333','000003333333331',
    NULL,'James','P','Macdonell',NULL,NULL,NULL,NULL,NULL,
   'James.Macdonell@example.edu',NULL,NULL,NULL,NULL,
   '2004-09-15 11:36:25.000',
   'FS Block 15 & 10 DD',25,'Students/Staff/Faculty',
   3,NULL,NULL,NULL,NULL,NULL,NULL,
   '2019-05-02 12:06:32.947',NULL,NULL,0,NULL,0,0,0,0,NULL,34567,NULL,NULL);

INSERT INTO AV_User_PCS_PatronAccount VALUES(
    'Points',3,30.00,0.00,0,NULL,NULL,NULL,
    0.00,0,0,0.00,30.00,NULL,NULL,34567,NULL);

INSERT INTO AV_User_PCS_PatronAccount VALUES(
    'Board',20,0.00,0.00,0,'2019-06-14 12:04:25.333',
    NULL,NULL,45.00,24,0,45.00,45.00,NULL,NULL,34567,NULL);

INSERT INTO AV_User_PCS_PatronAccount VALUES(
    'Flex Cash',4,0.00,0.00,0,NULL,NULL,NULL,
    0.00,0,0,0.00,0.00,NULL,NULL,34567,NULL);

INSERT INTO AV_User_PCS_PatronAccount VALUES(
    'Aux.  Points',5,0.00,0.00,0,NULL,NULL,NULL,
    0.00,0,0,0.00,0.00,NULL,NULL,34567,NULL);

INSERT INTO AV_User_PCS_PatronAccount VALUES(
    'SDX Dining Dollars',11,0.00,0.00,0,NULL,NULL,NULL,
    0.00,0,0,0.00,0.00,NULL,NULL,34567,NULL);

INSERT INTO AV_User_PCS_PatronAccount VALUES(
    'Meal Swipes',13,0.00,0.00,0,NULL,NULL,NULL,
    0.00,0,0,0.00,0.00,NULL,NULL,34567,NULL);

INSERT INTO AV_User_PCS_PatronAccount VALUES(
    'SDX DD Enrichment',12,0.00,0.00,0,NULL,NULL,NULL,
    0.00,0,0,0.00,0.00,NULL,NULL,34567,NULL);

-- Someone with a fractional meal swipe
INSERT INTO AV_User_PCS_Patron VALUES('444444444','000004444444441',
    NULL,'Hubert','J','Fransworth',NULL,NULL,NULL,NULL,NULL,
   'Hubert.Farnsworth@example.edu',NULL,NULL,NULL,NULL,
   '2004-09-15 11:36:25.000',
   'FS Block 15 & 10 DD',25,'Students/Staff/Faculty',
   3,NULL,NULL,NULL,NULL,NULL,NULL,
   '2019-05-02 12:06:32.947',NULL,NULL,0,NULL,0,0,0,0,NULL,87654,NULL,NULL);

INSERT INTO AV_User_PCS_PatronAccount VALUES(
    'Points',3,30.00,0.00,0,NULL,NULL,NULL,
    0.00,0,0,0.00,30.00,NULL,NULL,87654,NULL);

INSERT INTO AV_User_PCS_PatronAccount VALUES(
    'Board',20,3.18,0.00,0,'2019-06-13 11:34:23.103',
    NULL,NULL,45.00,24,0,45.00,45.00,NULL,NULL,87654,NULL);
