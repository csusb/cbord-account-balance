import os
import tempfile

import pytest

from account_balance import db, app as account_balance_app
from account_balance.models import Patron, PatronAccount

with open(os.path.join(os.path.dirname(__file__), 'data.sql'),'rb') as f:
    _data_sql = f.read().decode('utf8')

with open(os.path.join(os.path.dirname(__file__), 'pictures.sql'),'rb') as f:
    _pictures_sql = f.read().decode('utf8')

@pytest.fixture
def app():
   db_fd, db_path = tempfile.mkstemp()
   account_balance_app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///" + db_path

   db.create_all()
   connection = db.engine.raw_connection()
   try:
      cursor = connection.cursor()
      cursor.executescript(_data_sql)
      cursor.executescript(_pictures_sql)
      cursor.close()
      connection.commit()
   finally:
      connection.close()

   yield account_balance_app
   os.close(db_fd)
   os.unlink(db_path)

